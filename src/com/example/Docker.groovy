#!/usr/bin/env groovy

package com.example

class Docker implements Serializable {
    def script

    Docker(script){
        this.script = script
    }
    

    def buildDockerImage (String IMAGE_NAME){
    script.sh "echo Building the docker image"
    script.sh "docker build -t $IMAGE_NAME ."
        
    }

    def dockerLogin(){
        script.sh "echo Building the docker image"
        script.withCredentials([script.usernamePassword(credentialsId: 'dockertoken', passwordVariable: 'PASSWORD', usernameVariable: 'USERNAME')]) {
            script.sh "echo $script.PASSWORD |docker login -u $script.USERNAME --password-stdin"
        }
    }

    def dockerPush(String IMAGE_NAME){
       script.sh "docker push $IMAGE_NAME"
    }

    
}