#!/usr/bin/env groovy

def call (){
    sh "echo building the Jar package for $BRANCH_NAME"
    sh "mvn clean package"
}